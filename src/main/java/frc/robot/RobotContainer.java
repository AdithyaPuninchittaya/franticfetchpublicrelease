// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.controller.PIDController;
import edu.wpi.first.wpilibj.controller.RamseteController;
import edu.wpi.first.wpilibj.controller.SimpleMotorFeedforward;
import edu.wpi.first.wpilibj.geometry.Pose2d;
import edu.wpi.first.wpilibj.geometry.Rotation2d;
import edu.wpi.first.wpilibj.trajectory.Trajectory;
import edu.wpi.first.wpilibj.trajectory.TrajectoryConfig;
import edu.wpi.first.wpilibj.trajectory.TrajectoryGenerator;
import edu.wpi.first.wpilibj.trajectory.constraint.DifferentialDriveVoltageConstraint;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.RamseteCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import frc.robot.subsystems.RomiDrivetrain;

import java.util.Arrays;
import java.util.List;

/**
 * This class is where the bulk of the robot should be declared. Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls). Instead, the structure of the robot (including
 * subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {
    private final XboxController driver = new XboxController(0);
    // The robot's subsystems and commands are defined here...
    private final RomiDrivetrain m_romiDrivetrain = new RomiDrivetrain();

    private final Command autonomousCommand;

    /**
     * The container for the robot. Contains subsystems, OI devices, and commands.
     */
    public RobotContainer() {
        autonomousCommand = new SequentialCommandGroup(
                ramseteCommandHelper(Arrays.asList(
                        new Pose2d(0, 0, new Rotation2d(0)),
                        new Pose2d(0.32, 0.4, new Rotation2d(100 * Math.PI / 180))
                ), false),
                ramseteCommandHelper(Arrays.asList(
                        new Pose2d(0, 0, new Rotation2d(0)),
                        new Pose2d(-0.53, -0.27, new Rotation2d(30 * Math.PI / 180)),
                        new Pose2d(-0.57, -0.97, new Rotation2d(80 * Math.PI / 180))
                ), true),
                ramseteCommandHelper(Arrays.asList(
                        new Pose2d(0, 0, new Rotation2d(-5 * Math.PI / 180)),
                        new Pose2d(0.26, -0.22, new Rotation2d(-90 * Math.PI / 180)),
                        new Pose2d(0.48, -0.67, new Rotation2d(-90 * Math.PI / 180))
                ), false),
                ramseteCommandHelper(Arrays.asList(
                        new Pose2d(0, 0, new Rotation2d(0)),
                        new Pose2d(-0.53, 0.0, new Rotation2d(30 * Math.PI / 180)),
                        new Pose2d(-0.63, -0.97, new Rotation2d(75 * Math.PI / 180))
                ), true),
                ramseteCommandHelper(Arrays.asList(
                        new Pose2d(0, 0, new Rotation2d(-20 * Math.PI / 180)),
                        new Pose2d(0.22, -0.22, new Rotation2d(-90 * Math.PI / 180)),
                        new Pose2d(0.27, -0.78, new Rotation2d(-90 * Math.PI / 180))
                ), false),
                ramseteCommandHelper(Arrays.asList(
                        new Pose2d(0, 0, new Rotation2d(-10 * Math.PI / 180)),
                        new Pose2d(-0.45, -0.40, new Rotation2d(70 * Math.PI / 180))
                ), true));
        // Configure the button bindings
        configureButtonBindings();
    }

    /**
     * Use this method to define your button->command mappings. Buttons can be created by
     * instantiating a {@link GenericHID} or one of its subclasses ({@link
     * edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then passing it to a {@link
     * edu.wpi.first.wpilibj2.command.button.JoystickButton}.
     */
    private void configureButtonBindings() {
        new JoystickButton(driver, XboxController.Button.kA.value)
                .whenHeld(getAutonomousCommand());
    }

    /**
     * Helper method to simplify the creation of a RamseteCommand that will
     * execute the given list of waypoints.
     *
     * @param waypoints  the waypoints to drive through
     * @param isReversed whether the robot executes a forward or reverse trajectory
     * @return a command group that includes a RamseteCommand that drives through
     * the given points
     */
    private Command ramseteCommandHelper(List<Pose2d> waypoints, boolean isReversed) {
        // Create a voltage constraint to ensure we don't accelerate too fast
        DifferentialDriveVoltageConstraint autoVoltageConstraint =
                new DifferentialDriveVoltageConstraint(
                        new SimpleMotorFeedforward(Constants.RomiDrivetrain.ksVolts,
                                Constants.RomiDrivetrain.kvVoltSecondsPerMeter,
                                Constants.RomiDrivetrain.kaVoltSecondsSquaredPerMeter),
                        Constants.RomiDrivetrain.kDriveKinematics,
                        Constants.RomiDrivetrain.kTrajectoryMaxVoltage);

        // Create config for trajectory
        TrajectoryConfig config = new TrajectoryConfig(Constants.RomiDrivetrain.kMaxSpeedMetersPerSecond,
                Constants.RomiDrivetrain.kMaxAccelerationMetersPerSecondSquared)
                .setKinematics(Constants.RomiDrivetrain.kDriveKinematics)
                .addConstraint(autoVoltageConstraint)
                .setReversed(isReversed);

        // Create trajectory from the given waypoints
        Trajectory trajectory = TrajectoryGenerator.generateTrajectory(waypoints, config);

        RamseteCommand ramseteCommand = new RamseteCommand(
                trajectory,
                m_romiDrivetrain::getPose,
                new RamseteController(Constants.RomiDrivetrain.kRamseteB,
                        Constants.RomiDrivetrain.kRamseteZeta),
                new SimpleMotorFeedforward(Constants.RomiDrivetrain.ksVolts,
                        Constants.RomiDrivetrain.kvVoltSecondsPerMeter,
                        Constants.RomiDrivetrain.kaVoltSecondsSquaredPerMeter),
                Constants.RomiDrivetrain.kDriveKinematics,
                m_romiDrivetrain::getWheelSpeeds,
                new PIDController(Constants.RomiDrivetrain.kPDriveVel, 0, 0),
                new PIDController(Constants.RomiDrivetrain.kPDriveVel, 0, 0),
                // RamseteCommand passes volts to the callback
                m_romiDrivetrain::tankDriveVolts,
                m_romiDrivetrain
        );

        return new InstantCommand(() -> m_romiDrivetrain.resetOdometry(trajectory.getInitialPose()), m_romiDrivetrain)
                .andThen(ramseteCommand)
                .andThen(() -> m_romiDrivetrain.tankDriveVolts(0, 0));
    }

    /**
     * Use this to pass the autonomous command to the main {@link Robot} class.
     *
     * @return the command to run in autonomous
     */
    public Command getAutonomousCommand() {
        return autonomousCommand;
    }
}